<?php

namespace App\Observers;

use App\Models\Order;
use Illuminate\Support\Facades\Http;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        if ( $order->status == 'processed' ) {
            if ( $order->hook_sent == 0 ) {
                $order->hook_sent = 1;
                $order->save();
                /* не передаем статус, так как итак понятно какой */
                $data = [
                    'id' => $order->id,
                    'user_id' => $order->user_id,
                    'updated_at' => $order->updated_at,
                ];
                Http::post('https://httpbin.org/post', $data);
            }
        }
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
