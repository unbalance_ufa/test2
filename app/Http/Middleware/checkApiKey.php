<?php

namespace App\Http\Middleware;

use App\Services\APiKeyService;
use Closure;

class checkApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $all = $request->all();
        $key = $all['key'] ?? null;
        $data = [];
        $data['error'] = 'Error key';
        if ( empty($key) ) {
            return response()->json($data, 401);
        }
        $apiKeyService = new APiKeyService();
        if ( !$apiKeyService->check($key) ) {
            return response()->json($data, 401);
        }
        return $next($request);
    }
}
