<?php

namespace App\Http\Controllers;

use App\Http\Middleware\checkApiKey;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\StatusOrderRequest;
use App\Repositories\OrderRepository;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(checkApiKey::class);
    }

    public function create(CreateOrderRequest $request)
    {
        $orderService = new OrderService();
        $order = $orderService->create($request->user_id);
        return $order->toJson();
    }

    public function listByUser(CreateOrderRequest $request)
    {
        $orderRep = new OrderRepository();
        return $orderRep->getByUserId($request->user_id);
    }

    public function setStatus(StatusOrderRequest $request)
    {
        $orderService = new OrderService();
        $order = $orderService->updateStatus($request->order_id, $request->status);
        if ( $order === false ) {
            return response()->json(['error' => 'already status'], 304);
        }
        return response()->json(['status' => 'ok'], 200);
    }
}
