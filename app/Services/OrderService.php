<?php

namespace App\Services;


use App\Models\Order;

class OrderService
{

    public function create($user_id)
    {
        $order = new Order();
        $order->user_id = $user_id;
        $order->status = 'new';
        $order->save();
        return $order;
    }

    public function updateStatus($order_id, $new_status)
    {
        $order = Order::find($order_id);
        if (empty($order)) {
            return false;
        }
        if ($order->status == $new_status) {
            return false;
        }
        $order->hook_sent = 0;
        $order->status = $new_status;
        $order->save();
        return $order;
    }
}
