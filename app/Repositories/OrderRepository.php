<?php

namespace App\Repositories;


use App\Models\Order;

class OrderRepository
{

    public function getByUserId($user_id)
    {
        /* стандартная пагинация от laravel.  */
        $select = [
            'id',
            'status',
            'created_at',
            'updated_at',
        ];
        return Order::select($select)
            ->where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->paginate(30);
    }

}
