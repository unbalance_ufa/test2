<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        for ($i = 0; $i < 10; $i++)
            DB::table('users')->insert([
                'name' => 'test' . $i,
                'email' => 'test'.$i.'@test.com',
                'password' => Hash::make('123'),
            ]);

    }
}
